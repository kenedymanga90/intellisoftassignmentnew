package employeesystem.demo.repository;

import employeesystem.demo.entities.Contract;
import employeesystem.demo.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
    @Transactional
    @Modifying
    @Query("UPDATE Department dt set dt.departname = :department where dt.pkid = :id ")
    public Integer Update (@Param("department") String department, @Param("id") Integer id);
}
