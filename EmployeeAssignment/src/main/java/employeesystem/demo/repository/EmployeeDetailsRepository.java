package employeesystem.demo.repository;

import employeesystem.demo.entities.EmployeeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
public interface EmployeeDetailsRepository extends JpaRepository<EmployeeDetails, Integer> {
    @Transactional
    @Modifying
    @Query("UPDATE EmployeeDetails ed set ed.address = :address,ed.dateofbirth =:dob,ed.name= :name where ed.pkid = :id ")
    public Integer Update (@Param("address") String address, @Param("dob") Date dob,@Param("name") String name, @Param("id") Integer id);
}
