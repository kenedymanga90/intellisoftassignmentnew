package employeesystem.demo.repository;


import employeesystem.demo.entities.EmployeeLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface EmployeeLocationRepository  extends JpaRepository<EmployeeLocation, Integer> {
    @Transactional
    @Modifying
    @Query("UPDATE EmployeeLocation el set el.locationname = :locationname,el.locationstreet= :locationstreet where el.pkid = :id ")
    public Integer Update (@Param("locationname") String locationname, @Param("locationstreet") String locationstreet, @Param("id") Integer id);
}
