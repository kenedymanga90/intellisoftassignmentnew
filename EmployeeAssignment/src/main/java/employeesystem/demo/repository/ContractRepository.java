package employeesystem.demo.repository;

import employeesystem.demo.entities.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
public  interface ContractRepository extends JpaRepository<Contract, Integer> {

    @Transactional
    @Modifying
    @Query("UPDATE Contract cb set cb.assigneddate = :assigneddate,cb.contracttype= :contracttype,cb.expirydate = :expirydate where cb.pkid = :id ")
    public Integer Update (@Param("assigneddate") Date assigneddate,@Param("contracttype") String contracttype,@Param("expirydate") Date expirydate, @Param("id") Integer id);
}
