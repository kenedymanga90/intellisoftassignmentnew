package employeesystem.demo.entities;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "EmployeeDetails")
public class EmployeeDetails {

    private int pkid;
    private String address;
    private Date dateofbirth;
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PKID", unique = true, nullable = false)
    public int getPkid() {
        return pkid;
    }

    public void setPkid(int pkid) {
        this.pkid = pkid;
    }

    @Column(name = "Address", unique = true, nullable = false)
    public String getAddress() {
        return address;
    }


    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "DateOfBirth", unique = true, nullable = false)
    public Date getDateofbirth() {
        return dateofbirth;
    }


    public void setDateofbirth(Date dateofbirth) {
        this.dateofbirth = dateofbirth;
    }
    @Column(name = "Name", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
