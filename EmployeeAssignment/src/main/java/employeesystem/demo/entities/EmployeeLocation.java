package employeesystem.demo.entities;

import javax.persistence.*;

@Entity
@Table(name = "EmployeeLocation")
public class EmployeeLocation {

    private int pkid;
    private String locationname;
    private String locationstreet;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PKID", unique = true, nullable = false)
    public int getPkid() {
        return pkid;
    }

    public void setPkid(int pkid) {
        this.pkid = pkid;
    }

    @Column(name = "LocationName", unique = true, nullable = false)
    public String getLocationname() {
        return locationname;
    }

    public void setLocationname(String locationname) {
        this.locationname = locationname;
    }

    @Column(name = "LocationStreet", unique = true, nullable = false)
    public String getLocationstreet() {
        return locationstreet;
    }

    public void setLocationstreet(String locationstreet) {
        this.locationstreet = locationstreet;
    }
}
