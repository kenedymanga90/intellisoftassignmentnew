package employeesystem.demo.services;

import employeesystem.demo.entities.EmployeeLocation;
import employeesystem.demo.repository.EmployeeLocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service

public class EmployeeLocationServices {
    private EmployeeLocationRepository employeeLocationRepository;

@Autowired
public EmployeeLocationServices(EmployeeLocationRepository employeeLocationRepository) {
    this.employeeLocationRepository = employeeLocationRepository;
}

    public EmployeeLocation Save(EmployeeLocation employeeLocation){
        return employeeLocationRepository.save(employeeLocation);
    }


    public void Delete(EmployeeLocation employeeLocation){
        employeeLocationRepository.delete(employeeLocation);

    }

    @Transactional
    public Integer update(String location, String locationstreet, Integer id){
        return employeeLocationRepository.Update(location, locationstreet, id);
    }
}
