package employeesystem.demo.services;

import employeesystem.demo.entities.Department;
import employeesystem.demo.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class DepartmeService {
    private DepartmentRepository departmentrepository;

    @Autowired
    public DepartmeService(DepartmentRepository departmentRepository){
        this.departmentrepository = departmentRepository;
    }

    public Department Save(Department department){
        return departmentrepository.save(department);
    }

    public void Delete(Department department){
        departmentrepository.delete(department);
    }

    @Transactional
    public Integer update(String departmentname,  Integer id){
        return departmentrepository.Update(departmentname,  id);
    }


}
