package employeesystem.demo.services;


import employeesystem.demo.entities.Contract;
import employeesystem.demo.repository.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class ContractService {
    private ContractRepository contractRepository;

    @Autowired
    public ContractService(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    public Contract  Save(Contract contract){

        return contractRepository.save(contract);

    }

    public void Delete(Contract contract){
        contractRepository.delete(contract);

    }

    @Transactional
    public Integer update(Date assigneddate,String contracttype,Date expirydate,Integer id){
        return contractRepository.Update(assigneddate, contracttype, expirydate, id);
    }

}
