package employeesystem.demo.services;

import employeesystem.demo.entities.EmployeeDetails;
import employeesystem.demo.repository.EmployeeDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class EmployeeDetailsServices {
    private EmployeeDetailsRepository employeeDetailsRepository;

    @Autowired
    public EmployeeDetailsServices(EmployeeDetailsRepository employeeDetailsRepository) {
        this.employeeDetailsRepository = employeeDetailsRepository;
    }

    public EmployeeDetails Save(EmployeeDetails employeeDetails){
        return employeeDetailsRepository.save(employeeDetails);
    }

    public void Delete(EmployeeDetails employeeDetails){
        employeeDetailsRepository.delete(employeeDetails);

    }

    @Transactional
    public Integer update(String address, Date dob, String name, Integer id){
        return employeeDetailsRepository.Update(address, dob, name, id);
    }
}
