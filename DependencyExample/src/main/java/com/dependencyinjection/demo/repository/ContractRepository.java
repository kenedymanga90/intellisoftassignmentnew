package com.dependencyinjection.demo.repository;

import com.dependencyinjection.demo.entities.Contract;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractRepository extends JpaRepository<Contract,Integer> {
}
