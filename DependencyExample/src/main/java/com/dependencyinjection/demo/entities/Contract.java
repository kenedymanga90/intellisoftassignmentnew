package com.dependencyinjection.demo.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Contract")
public class Contract {
    private int pkid;
    private Date dateassigned;
    private Date expirydate;
    private int employeeid;
    private String contracttype;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PKID", unique = true, nullable = false)
    public int getPkid() {
        return pkid;
    }

    public void setPkid(int pkid) {
        this.pkid = pkid;
    }

    @Column(name = "DateAssigned", nullable = false)
    public Date getDateassigned() {
        return dateassigned;
    }

    public void setDateassigned(Date dateassigned) {
        this.dateassigned = dateassigned;
    }

    @Column(name = "ExpiryDate", nullable = false)
    public Date getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(Date expirydate) {
        this.expirydate = expirydate;
    }

    @Column(name = "EmployeeID", nullable = false)
    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    @Column(name = "ContractType", nullable = false)
    public String getContracttype() {
        return contracttype;
    }

    public void setContracttype(String contracttype) {
        this.contracttype = contracttype;
    }

    void print(){

        System.out.println(pkid+" "+employeeid+" "+contracttype);

        }
//        Example of Setter based Dependency Injection
}
