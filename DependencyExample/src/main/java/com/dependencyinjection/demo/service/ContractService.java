package com.dependencyinjection.demo.service;

import com.dependencyinjection.demo.entities.Contract;
import com.dependencyinjection.demo.repository.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;


public class ContractService {
    private ContractRepository contractRepository;

    @Autowired
//    Constructor Based Dependency Injection
    public ContractService(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    public Contract Save(Contract contract){

        return contractRepository.save(contract);

    }

    public void Delete(Contract contract){
        contractRepository.delete(contract);

    }
}
