USE [Employees]
GO

/****** Object:  Table [dbo].[Contract]    Script Date: 04/21/2018 14:58:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Contract](
	[PKID] [int] IDENTITY(1,1) NOT NULL,
	[DateAssigned] [date] NOT NULL,
	[ExpiryDate] [date] NULL,
	[EmployeeID] [int] NULL,
	[ContractType] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Contract]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EmployeeDetails] ([PKID])
GO


