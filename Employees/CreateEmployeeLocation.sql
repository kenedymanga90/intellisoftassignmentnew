USE [Employees]
GO

/****** Object:  Table [dbo].[EmployeeLocation]    Script Date: 04/21/2018 15:00:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmployeeLocation](
	[PKID] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [varchar](max) NULL,
	[LocationStreet] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


