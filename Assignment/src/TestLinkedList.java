import java.util.LinkedList;

public class TestLinkedList {
    public static void main (String []args){
        LinkedList<String> test = new LinkedList<String>();
//        add elements to the linked list
        test.add("Andrew 50");
        test.add("Victor 70");
        test.add("Catherine 80");
        System.out.println("These are marks scored by these students:"+test);
//        we want to add the first two elements
        test.addFirst("Shadrack 100");
        test.addLast("Gideon 45");
        System.out.println("These are marks scored by these students after remarking for students who had not submitted:"+test);
        test.removeFirst();
        test.removeLast();
        System.out.println("These are marks scored by these students after detecting Irregularirties:"+test);

    }
}
