import java.util.Arrays;
import java.util.Scanner;

public class AnagramAssignment {

    public static void AnagramCompare(){
        String str1, str2;
        int str1len1, str1len2;
        boolean equal = true;
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter text one:");
        str1 = reader.next();
        Scanner readertext = new Scanner(System.in);
        System.out.println("Enter text two:");
        str2 = readertext.next();
        str1len1 = str1.length();   //Get the length of the first string
        str1len2 = str2.length();   //Get the length of the second string

        if (str1len1== str1len2){
            equal = true;
        }else {
            char[] str1Array = str1.toLowerCase().toCharArray();
            char[] str2Array = str2.toLowerCase().toCharArray();
            Arrays.sort(str1Array);
            Arrays.sort(str2Array);
            equal = Arrays.equals(str1Array, str2Array);
        }

         if(equal){
             System.out.println("String "  + str1 + " and " + str2 + " are Anagrams" );
         }else {
             System.out.println("String " + str1 + " and " + str2 + " are not  Anagrams" );
         }


    }

    public static void main(String[] args)
    {
        AnagramCompare();
    }


}
