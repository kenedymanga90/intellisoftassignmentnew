import java.util.ArrayList;

public class ArrayListExample {
    public static void main(String[] args){
        ArrayList<String> myclass = new ArrayList<String>();
        myclass.add("Kenedy");
        myclass.add("Brian");
        myclass.add("Otieno");
        myclass.add("Mwangi");
        myclass.add("Felix");
        myclass.add("Maureen");
        System.out.println("These are my family members"+myclass);
        myclass.remove("Mwangi");
        myclass.remove("Felix");
        System.out.println("These are my family members who have remained after the two died"+myclass);
        myclass.add("Josephine");
        myclass.add("Jim");
        System.out.println("Afte giving birth to two more children now these are the kids"+myclass);

    }
}
