package com.example.testapplication.demo.repository;

import com.example.testapplication.demo.entity.Cafe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CafeRepository extends JpaRepository <Cafe,Integer>{
}
