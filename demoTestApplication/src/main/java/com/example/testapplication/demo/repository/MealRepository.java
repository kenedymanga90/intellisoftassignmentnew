package com.example.testapplication.demo.repository;

import com.example.testapplication.demo.entity.Meal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealRepository extends JpaRepository<Meal,Integer> {
}
