package com.example.testapplication.demo.entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table (name = "Cafe")
public class Cafe {
    private int pkid;
    private String cafename;
    private String cafelocation;
    private Date createdate;
    private Date lastupdate;
    private String createdby;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PKID", unique = true, nullable = false)
    public int getPkid() {
        return pkid;
    }


    public void setPkid(int pkid) {
        this.pkid = pkid;
    }

    @Column(name = "CafeName", unique = true, nullable = false, length = 50)
    public String getCafename() {
        return cafename;
    }

    public void setCafename(String cafename) {
        this.cafename = cafename;
    }

    @Column(name = "CafeLocation", unique = true, nullable = false, length = 50)
    public String getCafelocation() {
        return cafelocation;
    }

    public void setCafelocation(String cafelocation) {
        this.cafelocation = cafelocation;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", nullable = false, length = 23)
    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastUpdate", nullable = false, length = 23)
    public Date getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
    }

    @Column(name = "Createdby", unique = true, nullable = false, length = 50)
    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }
}
