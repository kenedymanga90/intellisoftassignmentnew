package com.example.testapplication.demo.entity;

import javax.persistence.*;

@Entity
@Table (name= "Meal")
public class Meal {
    private int PKID;
    private String mealname;
    private String dayserved;
    private Double amount;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PKID", unique = true, nullable = false)
    public int getPKID() {
        return PKID;
    }

    public void setPKID(int PKID) {
        this.PKID = PKID;
    }

    @Column(name = "Mealname", unique = true, nullable = false, length = 50)
    public String getMealname() {
        return mealname;
    }

    public void setMealname(String mealname) {
        this.mealname = mealname;
    }
    @Column(name = "DaySreved", unique = true, nullable = false, length = 50)
    public String getDayserved() {
        return dayserved;
    }

    public void setDayserved(String dayserved) {
        this.dayserved = dayserved;
    }

    @Column(name = "Amount", unique = true, nullable = false, length = 50)
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
